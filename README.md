# NN
## 有关神经网络的构建基本框架：L层-二维函数

### 假设在构建的过程中，我们设置的神经网络有L层（结合起来总共有L层的神经网络利用F_(L-layer)来表示）。我们的已有的training数据的形式为(x_1,y_1 ),…,(x_n,y_n)，其中x_i为2维数据，表示两种因子的因子值，y_i为一维数据，表示两种因子获得的因子收益（两种因子单独收益相加）。
### Loss Function=E(W_1,b_1,…,W_L,b_L )=1/2n ∑_(i=1)^n▒|(|F_(L-layer) (x_i )-y_i |)|^2 =1/2n ∑_(i=1)^n▒|(|N^L∘…∘N^1 (x_i )-y_i |)|^2 

# **Sigmoid函数的对象是什么？**
### 选取的sigmoid函数为：σ(x)=1/(1+e^(-x) )，其中(∂σ(x))/∂x=σ(x)(1-σ(x))。
### Sigmoid函数作用的对象是我们构建出来的关于x_i的线性组合，即：f_k (x)=W_k^T x+b_k。
### 其中N^K (x)=σ(x)∘f_k (x)=σ(x)∘(W_k^T x+b_k )=σ(W_k^T x+b_k )。

# **梯度下降函数的对象是什么？**
### 利用Adaptive Momentum Method进行每一步的优化（A more sophisticated method that adapts the steps size dynamically for each component of the gradient.）
### 梯度下降的对象为loss function，即：Loss Function=1/2n ∑_(i=1)^n▒|(|N^L∘…∘N^1 (x_i )-y_i |)|^2 。

# **交叉样本外要怎样实现？**
### 将我们已有的数据分成三份，第一份为training data，第二份作为cross-validation调整参数，第三部分用于样本内预测。

# **利用梯度下降函数的目标是什么？**
### 这个类似于最优解的求解，我们在每一步中求出loss function的梯度，然后依据这个梯度对W_k（权重）进行优化，让loss function朝着最快的下降方向移动，使得经过每一次优化，loss function的值变小，即预测值对于真实值的拟合更好。



